# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api
from odoo.addons.project.models.project import Task


class ProjectTask(models.Model):
    _inherit = 'project.task'

    @api.multi
    def message_get_suggested_recipients(self):
        recipients = super(Task, self).message_get_suggested_recipients()
        return recipients

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
