# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Project Task No customer suggestion',
    'summary': '',
    'version': '10.0',
    'category': 'Project',
    'author': 'BizzAppDev',
    'depends': [
        'project',
    ],
    'data': [
    ],
    'installable': True,
    'license': 'Other proprietary',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:       
